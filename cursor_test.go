// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package grub_test

import (
	"bytes"
	"errors"
	"testing"

	"gitlab.com/zygoon/go-grub"
)

func TestCursorEmptyBuf(t *testing.T) {
	cur := grub.NewCursor([]byte{})

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}
}

func TestCursorSkipsOverHeader(t *testing.T) {
	cur := grub.NewCursor([]byte("# header\n"))

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}
}

func TestCursorSkipsOverCorruptedHeader(t *testing.T) {
	cur := grub.NewCursor([]byte("# header"))

	if cur.Next() != false {
		t.Fatalf("Expected to find no no entries")
	}
}

func TestCursorSeesValidEntry(t *testing.T) {
	cur := grub.NewCursor([]byte("key=value\n"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if e := cur.Entry(); e != "key=value\n" {
		t.Fatalf("Unexpected entry: %q\n", e)
	}

	if e := cur.Key(); e != "key" {
		t.Fatalf("Unexpected entry key: %q\n", e)
	}

	if e := cur.Value(); e != "value" {
		t.Fatalf("Unexpected entry value: %q\n", e)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSeesUnterminatedRecord(t *testing.T) {
	cur := grub.NewCursor([]byte("key=value"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if e := cur.Entry(); e != "key=value" {
		t.Fatalf("Unexpected entry: %q\n", e)
	}

	if e := cur.Key(); e != "key" {
		t.Fatalf("Unexpected entry key: %q\n", e)
	}

	if e := cur.Value(); e != "value" {
		t.Fatalf("Unexpected entry value: %q\n", e)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorAdvancesThroughEntries(t *testing.T) {
	cur := grub.NewCursor([]byte("k1=v1\nk2=v2\n"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if e := cur.Entry(); e != "k1=v1\n" {
		t.Fatalf("Unexpected entry: %q\n", e)
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if e := cur.Entry(); e != "k2=v2\n" {
		t.Fatalf("Unexpected entry: %q\n", e)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only two entries")
	}
}

func TestCursorRecognizesUnusedSpace(t *testing.T) {
	cur := grub.NewCursor([]byte("# Header\nkey=value\n##########"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSetValueSameSize(t *testing.T) {
	// Note, the header is exactly ten bytes.
	cur := grub.NewCursor([]byte("# Header \nkey=1\n##########"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=1\n" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("0"); err != nil {
		t.Fatalf("Cannot set new value: %v", err)
	}

	if ent := cur.Entry(); ent != "key=0\n" {
		t.Fatalf("Unexpected entry after assignment: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSetValueEscaping(t *testing.T) {
	// Note, the header is exactly ten bytes.
	cur := grub.NewCursor([]byte("# Header \nkey=10\n#########"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=10\n" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("\n0"); err != nil {
		t.Fatalf("Cannot set new value: %v", err)
	}

	if ent := cur.Entry(); ent != "key=\\\n0\n" {
		t.Fatalf("Unexpected entry after assignment: %q", ent)
	}

	if err := cur.SetValue(`\`); err != nil {
		t.Fatalf("Cannot set new value: %v", err)
	}

	if ent := cur.Entry(); ent != "key=\\\\\n" {
		t.Fatalf("Unexpected entry after assignment: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSetValueSmallerSize(t *testing.T) {
	block := []byte("# Header\nkey=potato\nafter=pristine\n#")
	cur := grub.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=potato\n" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("foo"); err != nil {
		t.Fatalf("Cannot set new value: %v", err)
	}

	if ent := cur.Entry(); ent != "key=foo\n" {
		t.Fatalf("Unexpected entry after assignment: %q", ent)
	}

	if !bytes.Equal(block, []byte("# Header\nkey=foo\nafter=pristine\n####")) {
		t.Fatalf("Unexpected block after assignment: %q", string(block))
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "after=pristine\n" {
		t.Fatalf("Unexpected second entry: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only two entries")
	}
}

func TestCursorSetValueLargerSize(t *testing.T) {
	block := []byte("# Header\nkey=foo\nafter=pristine\n####")
	cur := grub.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=foo\n" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("potato"); err != nil {
		t.Fatalf("Cannot set new value: %v", err)
	}

	if ent := cur.Entry(); ent != "key=potato\n" {
		t.Fatalf("Unexpected entry after assignment: %q", ent)
	}

	if !bytes.Equal(block, []byte("# Header\nkey=potato\nafter=pristine\n#")) {
		t.Fatalf("Unexpected block after assignment: %q", string(block))
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "after=pristine\n" {
		t.Fatalf("Unexpected second entry: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only two entries")
	}
}

func TestCursorSetValueInsufficientSpace(t *testing.T) {
	block := []byte("# Header\nkey=123\nother=value\n#")
	cur := grub.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=123\n" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("12345"); !errors.Is(err, grub.ErrInsufficientSpace) {
		t.Fatalf("Unexpected success in setting the new value: %v", err)
	}

	if !bytes.Equal(block, []byte("# Header\nkey=123\nother=value\n#")) {
		t.Fatalf("Unexpected block after failed assignment: %q", string(block))
	}

	if ent := cur.Entry(); ent != "key=123\n" {
		t.Fatalf("Unexpected entry after failed assignment: %q", ent)
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry entry")
	}

	if ent := cur.Entry(); ent != "other=value\n" {
		t.Fatalf("Unexpected second entry after failed assignment: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSetValueCorruptedEntry(t *testing.T) {
	block := []byte("# Header\ncorrupted\n#")
	cur := grub.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "corrupted\n" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("potato"); !errors.Is(err, grub.ErrCorruptedEntry) {
		t.Fatalf("Unexpected success in setting the new value: %v", err)
	}

	if ent := cur.Entry(); ent != "corrupted\n" {
		t.Fatalf("Unexpected entry after failed assignment: %q", ent)
	}

	if !bytes.Equal(block, []byte("# Header\ncorrupted\n#")) {
		t.Fatalf("Unexpected block after failed assignment: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSetValueInvalidCursor(t *testing.T) {
	block := []byte("# Header\n#")
	cur := grub.NewCursor(block)

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}

	if err := cur.SetValue("foo"); !errors.Is(err, grub.ErrInvalidCursor) {
		t.Fatalf("Unexpected success in setting the new value: %v", err)
	}

	if !bytes.Equal(block, []byte("# Header\n#")) {
		t.Fatalf("Unexpected block after failed assignment: %q", string(block))
	}
}

func TestCursorDelete(t *testing.T) {
	block := []byte("# Header\nkey=foo\nafter=pristine\n####")
	cur := grub.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=foo\n" {
		t.Fatalf("Unexpected entry before deletion: %q", ent)
	}

	cur.Delete()

	if ent := cur.Entry(); ent != "" {
		t.Fatalf("Unexpected entry after deletion: %q", ent)
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "after=pristine\n" {
		t.Fatalf("Unexpected entry deletion: %q", ent)
	}

	if !bytes.Equal(block, []byte("# Header\nafter=pristine\n############")) {
		t.Fatalf("Unexpected block after deletion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestDeleteSaturated(t *testing.T) {
	block := []byte("# Header\nk=v\nkey=value\n")
	cur := grub.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "k=v\n" {
		t.Fatalf("Unexpected entry before deletion: %q", ent)
	}

	cur.Delete()

	if ent := cur.Entry(); ent != "" {
		t.Fatalf("Unexpected entry after deletion: %q", ent)
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "key=value\n" {
		t.Fatalf("Unexpected entry after deletion: %q", ent)
	}

	if !bytes.Equal(block, []byte("# Header\nkey=value\n####")) {
		t.Fatalf("Unexpected block after deletion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestInsertBeforeAtHeader(t *testing.T) {
	// This example shows incorrect usage of the Cursor which results in a
	// corrupted file.
	block := []byte("# Header\na=1\n####")
	cur := grub.NewCursor(block)

	if err := cur.InsertBefore("b", "2"); err != nil {
		t.Fatalf("Cannot insert entry: %v", err)
	}

	if ent := cur.Entry(); ent != "b=2\n" {
		t.Fatalf("Unexpected entry after insertion: %q", ent)
	}

	if !bytes.Equal(block, []byte("b=2\n# Header\na=1\n")) {
		t.Fatalf("Unexpected block after insertion: %q", string(block))
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "a=1\n" {
		t.Fatalf("Unexpected second entry after insertion: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only two entries")
	}
}

func TestInsertBeforeNotAtEnd(t *testing.T) {
	block := []byte("# Header\na=1\n####")
	cur := grub.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find one entry")
	}

	if ent := cur.Entry(); ent != "a=1\n" {
		t.Fatalf("Unexpected entry before insertion: %q", ent)
	}

	if err := cur.InsertBefore("b", "2"); err != nil {
		t.Fatalf("Cannot insert entry: %v", err)
	}

	if ent := cur.Entry(); ent != "b=2\n" {
		t.Fatalf("Unexpected entry after insertion: %q", ent)
	}

	if !bytes.Equal(block, []byte("# Header\nb=2\na=1\n")) {
		t.Fatalf("Unexpected block after insertion: %q", string(block))
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}
}

func TestInsertBeforeNotAtEndInsufficientSpace(t *testing.T) {
	block := []byte("# Header\na=1000\n#")
	cur := grub.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find one entry")
	}

	if ent := cur.Entry(); ent != "a=1000\n" {
		t.Fatalf("Unexpected entry before insertion: %q", ent)
	}

	if err := cur.InsertBefore("b", "20"); !errors.Is(err, grub.ErrInsufficientSpace) {
		t.Fatalf("Unexpected success to insert entry: %v", err)
	}

	if ent := cur.Entry(); ent != "a=1000\n" {
		t.Fatalf("Unexpected entry after failed insertion: %q", ent)
	}

	if !bytes.Equal(block, []byte("# Header\na=1000\n#")) {
		t.Fatalf("Unexpected block after failed insertion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestInsertBeforeAtEnd(t *testing.T) {
	block := []byte("####")
	cur := grub.NewCursor(block)

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}

	if err := cur.InsertBefore("k", "v"); err != nil {
		t.Fatalf("Cannot insert entry: %v", err)
	}

	if ent := cur.Entry(); ent != "k=v\n" {
		t.Fatalf("Unexpected entry after insertion: %q", ent)
	}

	if !bytes.Equal(block, []byte("k=v\n")) {
		t.Fatalf("Unexpected block after insertion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestInsertBeforeAtEndInsufficientUnusedSpace(t *testing.T) {
	block := []byte("###")
	cur := grub.NewCursor(block)

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}

	if err := cur.InsertBefore("k", "v"); !errors.Is(err, grub.ErrInsufficientSpace) {
		t.Fatalf("Unexpected success in inserting entry: %v\n", err)
	}

	if !bytes.Equal(block, []byte("###")) {
		t.Fatalf("Unexpected block after failed insertion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}
}

func TestEscaping(t *testing.T) {
	cur := grub.NewCursor([]byte("key=multi\\\nline\nkey2=\\\\\n"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if e := cur.Entry(); e != "key=multi\\\nline\n" {
		t.Fatalf("Unexpected entry: %q\n", e)
	}

	if e := cur.Key(); e != "key" {
		t.Fatalf("Unexpected entry key: %q\n", e)
	}

	if e := cur.Value(); e != "multi\nline" {
		t.Fatalf("Unexpected entry value: %q\n", e)
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if e := cur.Entry(); e != "key2=\\\\\n" {
		t.Fatalf("Unexpected entry: %q\n", e)
	}

	if e := cur.Key(); e != "key2" {
		t.Fatalf("Unexpected entry key: %q\n", e)
	}

	if e := cur.Value(); e != `\` {
		t.Fatalf("Unexpected entry value: %q\n", e)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only two entries")
	}
}

func TestCorruptedEscapeSequence(t *testing.T) {
	cur := grub.NewCursor([]byte("key=corrupt\\"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if e := cur.Entry(); e != "key=corrupt\\" {
		t.Fatalf("Unexpected entry: %q\n", e)
	}

	if e := cur.Key(); e != "key" {
		t.Fatalf("Unexpected entry key: %q\n", e)
	}

	if e := cur.Value(); e != "corrupt" {
		t.Fatalf("Unexpected entry value: %q\n", e)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}
