// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package grub_test

import (
	"bytes"
	"errors"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/zygoon/go-grub"
)

func TestSmokeSysOTA(t *testing.T) {
	env := grub.NewEnv()

	if err := env.Set("SYSOTA_MODE", "normal"); err != nil {
		t.Fatalf("Unexpectedly failed to set SYSOTA_MODE: %v", err)
	}

	if err := env.Set("SYSOTA_ACTIVE", "A"); err != nil {
		t.Fatalf("Unexpectedly failed to set SYSOTA_ACTIVE: %v", err)
	}

	expectedPrefix1 := "# GRUB Environment Block\nSYSOTA_MODE=normal\nSYSOTA_ACTIVE=A\n#"
	if s := env.String(); !strings.HasPrefix(s, expectedPrefix1) {
		t.Fatalf("Unexpected result: %s", s)
	}

	if err := env.Set("SYSOTA_MODE", "try"); err != nil {
		t.Fatalf("Unexpectedly failed to set SYSOTA_MODE: %v", err)
	}

	expectedPrefix2 := "# GRUB Environment Block\nSYSOTA_MODE=try\nSYSOTA_ACTIVE=A\n#"
	if s := env.String(); !strings.HasPrefix(s, expectedPrefix2) {
		t.Fatalf("Unexpected result: %s", s)
	}
}

func TestSmoke(t *testing.T) {
	env := grub.NewEnv()

	if val, ok := env.Lookup("key"); ok || val != "" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if err := env.Set("key", "value"); err != nil {
		t.Fatalf("Unexpected failure to set key: %v", err)
	}

	if val, ok := env.Lookup("key"); !ok || val != "value" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if err := env.Set("key", "longer value"); err != nil {
		t.Fatalf("Unexpected failure to set key: %v", err)
	}

	if val, ok := env.Lookup("key"); !ok || val != "longer value" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if err := env.Set("key", "v"); err != nil {
		t.Fatalf("Unexpected failure to set key: %v", err)
	}

	if val, ok := env.Lookup("key"); !ok || val != "v" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if val := env.Get("key"); val != "v" {
		t.Fatalf("Unexpected get result: %v", val)
	}

	if err := env.Set("key", ""); err != nil {
		t.Fatalf("Unexpected failure to set key: %v", err)
	}

	if val, ok := env.Lookup("key"); !ok || val != "" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	env.Delete("key")

	if val, ok := env.Lookup("key"); ok || val != "" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if !bytes.Equal(env[:], grub.NewEnv()[:]) {
		t.Fatalf("Unexpected difference after modifications: %s", env)
	}
}

func TestDeleteDuplicateKeyName(t *testing.T) {
	env := grub.NewEnv()
	copy(env[len(grub.EnvHeader):], "key=one\nkey=two\nkey=three\n")

	if val, ok := env.Lookup("key"); !ok || val != "one" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	env.Delete("key")

	if val, ok := env.Lookup("key"); ok || val != "" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}
}

func TestSetDuplicateKeyName(t *testing.T) {
	env := grub.NewEnv()
	copy(env[len(grub.EnvHeader):], "key=one\nkey=two\nkey=three\n")

	if val, ok := env.Lookup("key"); !ok || val != "one" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if err := env.Set("key", "value"); err != nil {
		t.Fatalf("Unexpected failure to set value: %v", err)
	}

	if val, ok := env.Lookup("key"); !ok || val != "value" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	expected := grub.NewEnv()
	copy(expected[len(grub.EnvHeader):], "key=value\nkey=value\nkey=value\n")

	if !bytes.Equal(env[:], expected[:]) {
		t.Fatalf("Unexpected difference after modifications: %s", env)
	}
}

func TestLoadEnv(t *testing.T) {
	env, err := grub.LoadEnv("testdata/grubenv")
	if err != nil {
		t.Fatalf("Cannot open grubenv: %v", err)
	}

	if val, ok := env.Lookup("K1"); !ok || val != "multi\nline" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if val, ok := env.Lookup("K2"); !ok || val != "value" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if val, ok := env.Lookup("K3"); !ok || val != "" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if val, ok := env.Lookup(""); !ok || val != "potato" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}
}

func TestLoadEnvFailure(t *testing.T) {
	_, err := grub.LoadEnv("testdata/grubenv.corrupt")
	if !errors.Is(err, grub.ErrInvalidSize) {
		t.Fatalf("Unexpectedly opened corrupted environment file: %v", err)
	}

	_, err = grub.LoadEnv("testdata/missing")
	if !errors.Is(err, os.ErrNotExist) {
		t.Fatalf("Unexpectedly opened missing environment file: %v", err)
	}
}

func TestSaveEnv(t *testing.T) {
	env := grub.NewEnv()
	d := t.TempDir()

	if err := env.Save(filepath.Join(d, "grubenv")); err != nil {
		t.Fatalf("Unexpected failure to save environment: %v", err)
	}

	data, err := os.ReadFile(filepath.Join(d, "grubenv"))
	if err != nil {
		t.Fatalf("Unexpected failure to re-read environment: %v", err)
	}

	if !bytes.Equal(data, env[:]) {
		t.Fatalf("Unexpected difference saved environment: %q", data)
	}
}
