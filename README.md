<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# About the Go GRUB module

This module contains pure Go implementation of the GRUB environment block data
format.

Technically GRUB recommends that all interactions with `grubenv` are to be
performed by executing the  `grub-editenv` program. This does, however, put an
unnecessary burden on launching the new process, parsing the output and handling
errors. This format is not officially documented but is now well recognized and
stable since at least 2013.

Unlike the naive approach, extreme care is taken to match the original C
implementation.

## Overview of package

The `grub` package provides the high-level `Env` type and the low-level `Cursor`
type. The former provides functions for looking up, assigning and deleting keys
and is the recommended way to work with environment. The latter offers low-level
cursor access to navigate a `grubenv`-like byte slice and perform specific
operations. Use the cursor type to work with corrupted environment blocks of
where you may find duplicate keys.

## Contributions

Contributions are welcome. Please try to respect Go requirements (1.22 at the
moment) and the overall coding and testing style.

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making
it easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
