// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package main implements the grub-editenv program with sub-commands for
// manipulating entries of a grub environment block.
package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-grub"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"
)

type editEnvCmd struct{}

func (editEnvCmd) Run(ctx context.Context, args []string) error {
	r := router.Cmd{
		Name: "grub-editenv",
		Commands: map[string]cmdr.Cmd{
			"create": createCmd{},
			"list":   listCmd{},
			"get":    getCmd{},
			"set":    setCmd{},
			"del":    delCmd{},
		},
	}

	return r.Run(ctx, args)
}

type createCmd struct{}

func (createCmd) OneLiner() string {
	return "create an empty environment block"
}

const (
	usageGRUBENV           = "Usage: %s GRUBENV\n"
	usageGRUBENV_KEY       = "Usage: %s GRUBENV KEY\n"
	usageGRUBENV_KEY_VALUE = "Usage: %s GRUBENV KEY VALUE\n"
)

func (createCmd) Run(ctx context.Context, args []string) error {
	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), usageGRUBENV, fs.Name())
	}

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 1 {
		fs.Usage()

		return flag.ErrHelp
	}

	env := grub.NewEnv()

	return env.Save(fs.Arg(0))
}

type listCmd struct{}

func (listCmd) OneLiner() string {
	return "list all variables"
}

func (listCmd) Run(ctx context.Context, args []string) error {
	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), usageGRUBENV, fs.Name())
	}

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 1 {
		fs.Usage()

		return flag.ErrHelp
	}

	env, err := grub.LoadEnv(fs.Arg(0))
	if err != nil {
		return err
	}

	cur := grub.NewCursor(env[:])
	for cur.Next() {
		_, _ = fmt.Printf("%s=%q\n", cur.Key(), cur.Value())
	}

	return nil
}

type getCmd struct{}

func (getCmd) OneLiner() string {
	return "get the value of a given variable"
}

func (getCmd) Run(ctx context.Context, args []string) error {
	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), usageGRUBENV_KEY, fs.Name())
	}

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 2 {
		fs.Usage()

		return flag.ErrHelp
	}

	env, err := grub.LoadEnv(fs.Arg(0))
	if err != nil {
		return err
	}

	_, _ = fmt.Printf("%s\n", env.Get(fs.Arg(1)))

	return nil
}

type setCmd struct{}

func (setCmd) OneLiner() string {
	return "set the value of the given variable"
}

func (setCmd) Run(ctx context.Context, args []string) error {
	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), usageGRUBENV_KEY_VALUE, fs.Name())
	}

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 3 {
		fs.Usage()

		return flag.ErrHelp
	}

	env, err := grub.LoadEnv(fs.Arg(0))
	if err != nil {
		return err
	}

	if err := env.Set(fs.Arg(1), fs.Arg(2)); err != nil {
		return err
	}

	return env.Save(fs.Arg(0))
}

type delCmd struct{}

func (delCmd) OneLiner() string {
	return "delete the given variable"
}

func (delCmd) Run(ctx context.Context, args []string) error {
	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), usageGRUBENV_KEY, fs.Name())
	}

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 2 {
		fs.Usage()

		return flag.ErrHelp
	}

	env, err := grub.LoadEnv(fs.Arg(0))
	if err != nil {
		return err
	}

	env.Delete(fs.Arg(1))

	return env.Save(fs.Arg(0))
}

func main() {
	cmdr.RunMain(editEnvCmd{}, os.Args)
}
