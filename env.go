// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package grub provides manipulation functions for GRUB environment block. Use
// OpenEnv to load an environment block. Use Env.Get, Env.Set, Env.Delete to
// manipulate entries. For low-level access use NewCursor to navigate the raw
// contents of the environment block file.
package grub

import (
	"errors"
	"os"
)

var (
	// ErrInvalidSize records attempts to open a grub environment block of size other than expected.
	ErrInvalidSize = errors.New("invalid environment size")
)

const (
	// EnvHeader is a fixed string at the beginning of the environment block.
	EnvHeader = "# GRUB Environment Block\n"
)

// Env is a GRUB environment block.
//
// GRUB environment block is a fixed-size byte array for storing key-value pairs
// terminated with a newline byte. Any byte may be escaped with the '\' byte.
// The block has a fixed size informative header which must be present. All
// unused space in the block is filled with '#' bytes
//
// Note that the zero value is not a valid environment block.
type Env [1024]byte

// NewEnv returns an empty, valid environment block.
func NewEnv() *Env {
	var env Env

	for i := 0; i < 1024; i++ {
		env[i] = '#'
	}
	copy(env[:], EnvHeader)

	return &env
}

// LoadEnv loads an environment block from the given file.
//
// LoadEnv fails if the file is not exactly 1024 bytes long.
func LoadEnv(path string) (*Env, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = f.Close()
	}()

	var env Env

	n, err := f.Read(env[:])
	if err != nil {
		return nil, err
	}

	if n != len(env) {
		return nil, ErrInvalidSize
	}

	var dummy [1]byte

	n, _ = f.Read(dummy[:])
	if n != 0 {
		return nil, ErrInvalidSize
	}

	return &env, nil
}

// Save saves the environment block to the given file.
//
// The file is written to without any fancy replacement logic. In case of power
// loss the original file may be truncated or clobbered. The file is created
// with mode 0o600 to avoid leaking information.
func (env *Env) Save(path string) error {
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0o600)
	if err != nil {
		return err
	}

	defer func() {
		_ = f.Close()
	}()

	n, err := f.Write(env[:])
	if err != nil {
		return err
	}

	if n != len(env) {
		return ErrInvalidSize
	}

	// Truncate the file in case it was longer than 1024 before.
	return f.Truncate(int64(len(env[:])))
}

func (env *Env) String() string {
	return string(env[:])
}

// Get returns the value of the given variable.
//
// If the variable is defined multiple times, the first value is returned.
// If the variable is not defined, an empty string is returned.
func (env *Env) Get(key string) string {
	v, _ := env.Lookup(key)
	return v
}

// Lookup returns the value of the given variable.
//
// Lookup is like Get except that it also returns a boolean flag indicating if
// the variable was defined or not.
func (env *Env) Lookup(key string) (string, bool) {
	cur := NewCursor(env[:])

	for cur.Next() {
		if cur.Key() == key {
			return cur.Value(), true
		}
	}

	return "", false
}

// Set sets the value of a given variable.
//
// Existing variable is resized in place, without affecting the order.
// If the variable is not defined it is inserted after all the existing
// variables.
//
// Set may fail if the value is too long to store. In that case the environment
// block is not modified.
//
// If there are multiple variables with the same name, they are all modified.
func (env *Env) Set(key, value string) error {
	cur := NewCursor(env[:])
	found := false

	for cur.Next() {
		if cur.Key() == key {
			found = true

			if err := cur.SetValue(value); err != nil {
				return err
			}
		}
	}

	if !found {
		return cur.InsertBefore(key, value)
	}

	return nil
}

// Delete deletes the given variable.
//
// If there are multiple variables with the same name, they are all deleted.
func (env *Env) Delete(key string) {
	cur := NewCursor(env[:])

	for cur.Next() {
		if cur.Key() == key {
			cur.Delete()
		}
	}
}
